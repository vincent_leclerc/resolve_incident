import models
from models import to_snake_case
import json


class Api(models.Api):

    def __init__(
            self,
            endpoint='https://api-sandbox.broadsign.com:10889/rest',
            token=None,
            additional_headers=dict()):
        super().__init__(endpoint, token, additional_headers)
        setattr(Resource, 'api', self)


class Resource(models.Resource):

    @classmethod
    def fetch_all(cls, domain_id=None, return_as='list', additional_url=None):
        """
        :param domain_id: int
        :param return_as: 'raw', 'dict' or 'list'
        :return: The resources called in the form requested by return_as.
        """
        class_name = to_snake_case(cls.__name__)
        if not additional_url:
            additional_url = '?domain_id='+str(domain_id) if domain_id else ''
        response = cls.api.call(
            class_name=class_name,
            http_method='get',
            version=cls.Info.version,
            additional_url=additional_url)
        response_body = cls.api.json(response)
        if return_as == 'raw':
            return response_body[class_name]
        elif return_as == 'dict':
            return {x['id']: cls(**x) for x in response_body[class_name]}
        return [cls(**x) for x in response_body[class_name]]

    @classmethod
    def fetch_list(cls, ids, domain_id=None, raw=False):
        class_name = to_snake_case(cls.__name__)
        additional_url = 'by_id?ids=' + ','.join(ids) + '&domain_id=' + str(domain_id) if domain_id else str(id)
        response = cls.api.call(
            class_name=class_name,
            http_method='get',
            version=cls.Info.version,
            additional_url=additional_url)
        response_body = cls.api.json(response)
        if raw:
            return response_body[class_name]
        else:
            instances = []
            for instance in response_body[class_name]:
                instances.append(cls(**instance))
            return instances

    @classmethod
    def fetch(cls, id, domain_id=None, raw=False):
        class_name = to_snake_case(cls.__name__)
        additional_url = 'by_id?ids=' + str(id) + '&domain_id=' + str(domain_id) if domain_id else str(id)
        response = cls.api.call(
            class_name=class_name,
            http_method='get',
            version=cls.Info.version,
            additional_url=additional_url)
        response_body = cls.api.json(response)
        if raw:
            return response_body[class_name][0]
        elif not response_body[class_name] == 0:
            return None
        else:
            return cls(**response_body[class_name][0])

    @classmethod
    def create_default(cls, domain_id=None):
        if domain_id:
            instance = cls(domain_id=domain_id)
        else:
            instance = cls()
        return instance.create(fetch_resource=True)

    def create(self, fetch_resource=False, force_update=False, full_create=True, **create_fields):
        class_name = to_snake_case(type(self).__name__)
        create_fields = {
            k: v
            for k, v
            in self.__dict__.items()
            if k in self.Info.required_create
               or (full_create and k in type(self).Info.optional_create)
               or (k in create_fields.keys() and k in type(self).Info.optional_create)}
        response = self.api.call(
            class_name=class_name,
            http_method='post',
            version=self.Info.version,
            additional_url='add',
            data=json.dumps(create_fields))
        response_body = self.api.json(response)
        id = response_body[class_name][0]['id']
        self.id = int(id)
        if force_update:
            self.update()
        if fetch_resource:
            return type(self).fetch(id=id, domain_id=self.domain_id)
        else:
            return id

    def update(self, full_update=False, *update_fields):
        class_name = to_snake_case(type(self).__name__)
        update_fields = {
            k: v
            for k, v
            in self.__dict__.items()
            if k in type(self).Info.required_update
               or ((full_update or (not full_update and k in update_fields))
                   and k in type(self).Info.optional_update)}
        response = self.api.call(
            class_name=class_name,
            http_method='put',
            version=self.Info.version,
            data=json.dumps(update_fields))
        response_body = self.api.json(response)



class Incident(Resource):
    critical_escalation_tm_utc = models.StringField()
    domain_id = models.IntegerField()
    escalation_status = models.IntegerField(choices={
        'Trivial': 1,
        'Warning': 2,
        'Critical': 3,
        'Resolved': 4}, default=1)
    id = models.IntegerField()
    last_status_code_change_utc = models.StringField()
    occurred_on = models.StringField()
    problem_description = models.StringField()
    reported_on = models.StringField()
    resolved_description = models.StringField()
    resolved_on = models.StringField()
    resource_id = models.IntegerField()
    status_code = models.IntegerField(choices={
        'Warning': 2,
        'Error': 3,
        'User intervention': 4,
        'Resolved': 5,
        'Resolved locally but not reported': 6,
        'Resolved but reported anyway': 7}, default=2)
    target_resource_id = models.IntegerField()
    type_code = models.IntegerField(choices={
        'Display device error': 2,
        'Display not fullscreen': 3,
        'Display resolution mismatch': 4,
        'Display temperature warning': 5,
        'Display temperature critical': 6,
        'Display orientation mismatch': 7,
        'Playback error': 103,
        'Playback missing file': 104,
        'Playback truncated file': 105,
        'Playback disk space': 106,
        'Playback expired file': 107,
        'Host/player Unexpected Shutdown': 205,
        'Host/player difficulties polling': 206,
        'Host/player performance problem': 207,
        'Host/player application fault': 208,
        'Host/player memory exhausted': 209,
        'Host/player deadlock': 210,
        'Host/player offline during business hours': 211,
        'Host/player URL sync failed': 212,
        'Host/player backup master activated': 213,
        'Domain quota exceeded': 305,
        'Host/player MIA': 204,
        '10001': 10001,
        '10002': 10002,
        '10003': 10003,
        '10004': 10004,
        '10005': 10005,
        '10006': 10006,
        '10007': 10007,
        '10008': 10008,
        '10009': 10009,
        '10010': 10010,
        '10011': 10011,
        '10012': 10012,
        '10013': 10013,
        '10014': 10014,
        '10015': 10015,
        '10016': 10016,
        '10017': 10017,
        '10018': 10018,
        '10019': 10019,
        '10020': 10020,
        '10021': 10021,
        '10022': 10022,
        '10023': 10023,
        '10024': 10024,
        '10025': 10025}, default=2)
    warning_escalation_tm_utc = models.StringField()

    @classmethod
    def fetch_by_types(cls, types, domain_id, raw=False):
        if type(types) == int:
            types = [types]
        class_name = to_snake_case(cls.__name__)
        additional_url = 'by_type?type_codes={}&domain_id={}'.format(
            ','.join([str(x) for x in types]),
            str(domain_id))

        response = cls.api.call(
            class_name=class_name,
            http_method='get',
            version=cls.Info.version,
            additional_url=additional_url)
        response_body = cls.api.json(response)
        if raw:
            return response_body['incident']
        return [cls(**x) for x in response_body['incident']]

    def manual_resolve(self, case_number):
        class_name = to_snake_case(self.__class__.__name__)
        fields = {
            'id': self.id,
            'domain_id': self.domain_id,
            'resolved_description': f'Automatically resolved by Broadsign Services for the case {case_number}.'}
        additional_url = 'manual_resolve'
        response = self.api.call(
            class_name=class_name,
            http_method='post',
            version=self.Info.version,
            additional_url=additional_url,
            data=json.dumps(fields))
        response_body = self.api.json(response)
        id = response_body[class_name][0]['id']
        return id

    class Info:
        version = 3
