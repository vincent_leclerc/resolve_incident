import re
import requests


class MetaResource(type):

    def __new__(mcs, name, bases, class_dict):
        _class = super().__new__(mcs, name, bases, class_dict)
        for k, v in _class.__dict__.items():
            if isinstance(v, Field):
                v.name = k
        return _class

    def __repr__(self):
        output = "\n\nclass {}:\n".format(self.__name__)
        for k, v in self.__dict__.items():
            if k != 'Info':
                output += '\t{} = {!r}\n'.format(k, v)
        if 'Info' in self.__dict__:
            output += '\n\tclass Info:'
            for k, v in self.Info.__dict__.items():
                if not k.startswith('__'):
                    output += '\n\t\t{} = {}'.format(k, v)
        return output


class Field(object):
    _type = object

    def __init__(self, default=None):
        if default is not None:
            if isinstance(default, self._type):
                self.default = default
            else:
                raise TypeError('Default value for field {} must be of type {}.\nValue: "{}"\nType: {}'
                                .format(self.name, self._type.__name__, default, type(default).__name__))
        else:
            self.default = self._type()

    def __set__(self, instance, value):
        if isinstance(value, self._type) or value is None:
            instance.__dict__[self.name] = value
        else:
            raise TypeError('Value for field {} must be of type {}.\nValue: "{}"\nType: {}'
                            .format(self.name, self._type.__name__, value, type(value).__name__))

    def __repr__(self):
        output = '{}({})'.format(self.__class__.__name__, dict_to_signature(self.__dict__))
        return output


class IntegerField(Field):
    _type = int

    def __init__(self, default=None, choices=None):
        super().__init__(default)
        if choices is not None and default not in choices.values():
            raise ValueError('Default value of IntegerField between be among choices.')
        else:
            self.choices = choices

    def __set__(self, instance, value):
        if self.choices and value not in self.choices.values():
            raise ValueError('Value of IntegerField between be among choices.')
        super().__set__(instance, value)


class StringField(Field):
    _type = str


class BooleanField(Field):
    _type = bool


class FloatField(Field):
    _type = float

    def __set__(self, instance, value):
        if isinstance(value, self._type):
            instance.__dict__[self.name] = value
        elif isinstance(value, int):
            instance.__dict__[self.name] = float(value)
        else:
            raise TypeError('Value for field {} must be of type {}.\nValue: "{}"\nType: {}'
                            .format(self.name, self._type.__name__, value, type(value).__name__))


def to_snake_case(string):
    return re.sub('([a-z0-9]+)([ ]?[A-Z])', r'\1_\2', string).lower().replace(' ', '')


class Resource(metaclass=MetaResource):

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls, *args)
        if 'Info' in cls.__dict__:
            required_fields = {
                k: v
                for k, v
                in cls.__dict__.items()
                if 'required_create' in cls.Info.__dict__
                    and k in cls.Info.required_create}
            for name, field in required_fields.items():
                if field.default is None and name not in kwargs.keys():
                    raise TypeError('Missing keyword argument {} in {} object instantiation.'.format(
                        name,
                        cls.__name__))

        for k, v in cls.__dict__.items():
            if isinstance(v, Field):
                if k not in kwargs.keys():
                    setattr(instance, k, v.default)
                else:
                    setattr(instance, k, kwargs[k])

        return instance

    @property
    def fields(self):
        return self.__class__.__dict__

    def __repr__(self):
        output = '{}({})'.format(self.__class__.__name__, dict_to_signature(self.__dict__))
        return output


class Api(object):
    """A class whose purposes are to:
    - Inject itself into the parent class "Resource" so that descendants can make calls to the API.
    - Parse swagger.json, create fully functional BCS Resource Classes and add them to the module."""

    def __init__(self, endpoint, token=None, additional_headers=dict()):
        self.endpoint = endpoint
        self.headers = {
            'Authorization': 'Bearer {}'.format(token),
            'cache-control': 'no-cache',
            'accept': 'application/json'}
        self.headers.update(additional_headers)

    def call(self, class_name, http_method, version, headers=dict(), additional_url=None, data=dict()):
        headers.update(self.headers)
        version = 'v' + str(version)
        url = '/'.join((self.endpoint, class_name, version))
        url += '/' + additional_url if additional_url else ''

        if data:
            return getattr(requests, http_method)(url, headers=headers, data=data)
        return getattr(requests, http_method)(url, headers=headers)

    @staticmethod
    def json(response):
        response_body = response.json()
        if 'status' in response_body and response_body['status'] == 'error':
            if 'error' in response_body:
                raise Exception(response_body['error'])
            elif 'message' in response_body:
                raise Exception(response_body['message'])
            else:
                raise Exception(response_body)
        return response_body


def dict_to_signature(_dict):
    output = ', '. join([dict_entry_to_parameter(k, v) for k, v in _dict.items() if not k.startswith('__')])
    return output


def dict_entry_to_parameter(k, v):
    return '{}={}'.format(k,'"'+v+'"' if type(v) == str else str(v))


