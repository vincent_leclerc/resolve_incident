import re
import control
from control import Api

TOKEN = ''
URL = 'api.broadsign.com:10889'
DOMAIN_ID = 1
CASE_NUMBER = 1


def main():
    Api(token=TOKEN, endpoint=f'https://{URL}/rest')
    incidents = control.Incident.fetch_by_types(types=10024, domain_id=DOMAIN_ID)

    for incident in incidents:
        print(incident)
        if incident.status_code < 5 and re.search(r'\d{3}:\d', incident.problem_description):
            print(incident.id, incident.target_resource_id, incident.manual_resolve(case_number=CASE_NUMBER))


if __name__ == '__main__':
    main()
